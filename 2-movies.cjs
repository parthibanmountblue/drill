const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/



//    Q1. Find all the movies with total earnings more than $500M. 

function movieEarnings(favouritesMovies) {
    let movieArray = Object.entries(favouritesMovies)
    let result = movieArray.filter(movie => {
        let movieEarnings = movie[1].totalEarnings
        let dollarRemove = movieEarnings.replace("$", "")
        let amountEarned = dollarRemove.replace("M", "")
        if (amountEarned >= 500) {
            return movie
        }
    })
    return result

}
let earningsOutput = movieEarnings(favouritesMovies);
console.log(earningsOutput)

//  Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

function oscarAndEarning(favouritesMovies) {
    let movieArray = Object.entries(favouritesMovies)
    let result = movieArray.filter(movie => {
        let movieEarnings = movie[1].totalEarnings
        let dollarRemove = movieEarnings.replace("$", "")
        let amountEarned = dollarRemove.replace("M", "")
        if ((amountEarned >= 500) && (movie[1].oscarNominations >= 4)) {
            return movie
        }
    })
    return result
}
let oscarAndEarningOutput = oscarAndEarning(favouritesMovies)
console.log(oscarAndEarningOutput)

//    Q.3 Find all movies of the actor "Leonardo Dicaprio".

function findingActor(favouritesMovies) {
    let movieArray = Object.entries(favouritesMovies)
    let result = movieArray.filter(movie => {
        let actorsArray = movie[1].actors
        if (actorsArray.includes("Leonardo Dicaprio")) {
            return movie

        }
    })
    return result
}
let myOutput = findingActor(favouritesMovies)
console.log(myOutput)

//  Group movies based on genre. Priority of genres in case of multiple genres present are:

function groupingGenre(favouritesMovies) {
    let movieArray = Object.entries(favouritesMovies)
    let myObj = {};
    let result = movieArray.forEach(element => {
        let genreArray = element[1].genre;
        genreArray.forEach(genre => {
            if (myObj.hasOwnProperty(genre)) {
                myObj[genre] += " "
                myObj[genre] += element[0]
            } else {
                myObj[genre] = ""
                myObj[genre] += element[0]
            }
        })
    })
    return myObj
}



let genregrouping = groupingGenre(favouritesMovies)
console.log(genregrouping)

// 4 Sort movies (based on IMDB rating)

function sorting(favouritesMovies) {
    let movieArray = Object.entries(favouritesMovies)
    let result = movieArray.sort(
        (p1, p2) =>
            (p1[1].imdbRating < p2[1].imdbRating) ? 1 : (p1[1].imdbRating > p2[1].imdbRating) ? -1 : 0);
    return result

}
let sortOutput = sorting(favouritesMovies)
console.log(sortOutput)