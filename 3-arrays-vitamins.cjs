const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];

//    1. Get all items that are available 
function isAvailable(items) {
    let availableItems = items.filter(elements => elements.available === true);
    console.log(availableItems);
}
isAvailable(items)

//   2. Get all items containing only Vitamin C.

function searchVitamins(items){
    let onlyVitaminc = items.filter(elements => elements.contains === "Vitamin C")
    console.log(onlyVitaminc)
}
searchVitamins(items)

//   3. Get all items containing Vitamin A.
function searchVitaminsA(items){
    let vitaminA = items.filter(elements => {
        if(elements.contains.includes("Vitamin A")){
            return elements
        }
    })
    console.log(vitaminA)
}
searchVitaminsA(items)


//  4. Group items based on the Vitamins that they contain

function group(items){
    let myObj ={};
    items.forEach(element => {
        myObj[element.name]=element.contains
        
    });
    console.log(myObj)

}
group(items)