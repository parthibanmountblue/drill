const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file*/



function priceGreater(products) {
    let myProducts=products[0]
    let arrayProduct=Object.entries(myProducts)
    let findPrice = arrayProduct.reduce((accumulator,currentvalue)=>{
        let value=currentvalue[1]
        let price = value.price
        if(typeof price!=="undefined" ){
            price=price.replace("$","")
            newPrice = parseFloat(price)
            if(newPrice>65){
                accumulator[currentvalue[0]]=currentvalue[1]
            }
        }else{
            value.forEach(item=>{
                let key = Object.keys(item)
                let value = Object.values(item)
                let price = item[key].price.replace("$","")
                let newPrice = parseFloat(price)
                if(newPrice>65){
                    accumulator[key]=value
                }
            })
        }
        return accumulator
    },{})
    return findPrice
   

}

let myresult = priceGreater(products);
console.log(myresult)


function quantity(products) {

    let myProducts = products[0]
    let arrayQuantity = Object.entries(myProducts)
    let findQuantity = arrayQuantity.reduce((accumulator, currentvalue) => {
        let value = currentvalue[1]
        let quantity = value.quantity
        if (typeof quantity !== "undefined") {
            if (quantity > 1) {
                accumulator[currentvalue[0]] = currentvalue[1]
            }
        } else {
            value.forEach(item => {
                let key = Object.keys(item)
                let value = Object.values(item)
                let quantity = item[key].quantity
                if (quantity > 1) {
                    accumulator[key] = value
                }
            })
        }
        return accumulator
    }, {})
    return findQuantity
}
let result = quantity(products)
console.log(result)





