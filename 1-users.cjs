const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

// Q4 Find all users with masters Degree.

function myDegree(users){
    let myValues = Object.entries(users)
    let result = myValues.filter(user => user[1].qualification === "Masters")
    console.log(result)
}
myDegree(users)

// Q2 Find all users staying in Germany.

function location(users){
    let myValues = Object.entries(users)
    let result = myValues.filter(user => user[1].nationality ==="Germany")
    console.log(result)
}
location(users)

//Q1 Find all users who are interested in playing video games.
function videoGames(users){
    let myValues = Object.entries(users)
    let result = myValues.filter(user => user[1].interests == "Video Games")
    console.log(result)
}
videoGames(users)

// Q5 Group users based on their Programming language mentioned in their designation.


function grouping(users){
    let myValues = Object.entries(users)
    let myObj = {};
    myObj.javascript=""
    myObj.python=""
    myObj.golang =""
    myValues.forEach(user => {
        let myString = user[1].desgination;
        if(myString.includes("Javascript") == true){
            myObj.javascript +=user[0]
            myObj.javascript +=" "
        }
        else if(myString.includes("Python")== true){
            myObj.python +=user[0]
            myObj.python +=" "
        }
        else if(myString.includes("Golang")== true){
            myObj.golang +=user[0]
            myObj.golang +=" "
        }

    })
    console.log(myObj)
}
grouping(users)

// Q3 Sort users based on their seniority level 
function seniority(users){
    let myValues = Object.entries(users)
    console.log(myValues)
}
seniority(users)